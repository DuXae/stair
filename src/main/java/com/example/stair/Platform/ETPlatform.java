package com.example.stair.Platform;

public class ETPlatform {
    public static int angleMax = 30;
    public static int clearanceOnWall = 50;
    public static int clearanceOnStepSupports = 100;
    public static int overlapRamp = 30;
    public static int catcher = 20;
    public static int lengthWayMax = 10000;
    public static int clearanceOnStep = 50;
    public static int lengthRamp= 215;
    public static int lengthPassing= 903 + 215;
    public static int lengthSide= 990;
    public static int widthOnWallPassing = 1068;
    public static int widthOnWallSide = 1206;
    public static int widthOnWallLowerSide = 1276;
    public static int widthOnSupportsPassing = 1260;
    public static int widthOnSupportsLowerSide = 1467;
    public static int widthOnSupportsSide = 1397;
    public static int foldedSupports = 600;
    public static int foldedWall = 420;

    public static int passage = 1200;

    public static int lengthTrackAdd = 1300;
}
