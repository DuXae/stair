package com.example.stair.Platform;

public class NPUPlatform {
    public static int angleMax = 30;
    public static int lengthWayMax = 10000;
    public static int lengthWayMaxDU = 6000;
    public static int clearanceOnWall = 50;
    public static int clearanceOnStepSupports = 100;
    public static int overlapRamp = 30;
    public static int clearanceOnStep = 40;
    public static int lengthRamp = 171;
    public static int lengthPassing = 990 + 171;
    public static int lengthSide = 990 + 40;
    public static int widthOnWallPassingLower = 1130;
    public static int widthOnWallSideLower = 1195;
    public static int widthOnSupportsPassingLower = 1235;
    public static int widthOnSupportsSideLower = 1300;
    public static int minUpperPlatformLower = 250;
    public static int widthOnWallPassingUpper = 1069;
    public static int widthOnWallSideUpper = 1134;
    public static int widthOnSupportsPassingUpper = 1235;
    public static int widthOnSupportsSideUpper = 1300;
    public static int minUpperPlatformUpper = 450;
    public static int foldedSupports = 470;
    public static int foldedWallLowerBox = 370;
    public static int foldedWallUpperBox = 310;
    public static int passage = 1200;
}
