package com.example.stair;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.example.stair.Platform.NPUPlatform;
import com.example.stair.model.PlatformModel;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class NPUImgController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button backButton;

    @FXML
    private Text angleStair;

    @FXML
    private Text angleStair1;

    @FXML
    private Text angleWay;

    @FXML
    private Text angleWay1;

    @FXML
    private Text heightStair;

    @FXML
    private Text heightStair1;

    @FXML
    private Text lengthStair;

    @FXML
    private Text lengthStair1;

    @FXML
    private Text lengthWayStair;

    @FXML
    private Text lengthWayStair1;

    @FXML
    private Text lengthUpperPlatform;

    @FXML
    private Text lengthUpperPlatform1;

    @FXML
    private Text minUpperPlatformLower;

    @FXML
    private Text minUpperPlatformUpper;

    @FXML
    private Text lengthWay;

    @FXML
    private Text lengthWay1;

    @FXML
    private Text lengthSide;

    @FXML
    private Text lengthSide1;

    @FXML
    private Text lengthLowerPlatform;

    @FXML
    private Text lengthLowerPlatform1;

    @FXML
    private Text lengthPassing;

    @FXML
    private Text lengthPassing1;

    @FXML
    private Text clearanceOnWall;

    @FXML
    private Text clearanceOnWall1;

    @FXML
    private Text clearanceOnWall2;

    @FXML
    private Text clearanceOnWall3;

    @FXML
    private Text clearanceOnWall4;

    @FXML
    private Text clearanceOnWall5;

    @FXML
    private Text clearanceOnWall6;

    @FXML
    private Text clearanceOnWall7;

    @FXML
    private Text clearanceOnStepSupport;

    @FXML
    private Text clearanceOnStepSupport1;

    @FXML
    private Text clearanceOnStepSupport2;

    @FXML
    private Text foldedSupports;

    @FXML
    private Text foldedWallLowerBox;

    @FXML
    private Text foldedWallUpperBox;

    @FXML
    private Text clearanceOnStepMax;

    @FXML
    private Text clearanceOnStepMaxNum;

    @FXML
    private Text clearanceOnStepMin;

    @FXML
    private Text clearanceOnStepMinNum;

    @FXML
    private Text widthPassingOnSupportLower;

    @FXML
    private Text widthPassingOnSupportUpper;

    @FXML
    private Text widthPassingOnWallLower;

    @FXML
    private Text widthPassingOnWallUpper;

    @FXML
    private Text widthStair;

    @FXML
    private Text widthStair1;

    @FXML
    private Text widthStair2;

    @FXML
    private Text widthStair3;

    @FXML
    private Text widthStair4;

    @FXML
    private Text widthStair5;

    @FXML
    private Text widthStair6;

    @FXML
    private Text widthSideOnSupportLower;

    @FXML
    private Text widthSideOnSupportUpper;

    @FXML
    private Text widthSideOnWallLower;

    @FXML
    private Text widthSideOnWallUpper;

    @FXML
    void initialize() {
        PlatformModel model = new PlatformModel();
        model.startModel();

        angleStair.setText(String.format("%.1f", model.angle));
        angleStair1.setText(String.format("%.1f", model.angle));

        clearanceOnWall.setText(String.valueOf(NPUPlatform.clearanceOnWall));
        clearanceOnWall1.setText(String.valueOf(NPUPlatform.clearanceOnWall));
        clearanceOnWall2.setText(String.valueOf(NPUPlatform.clearanceOnWall));
        clearanceOnWall3.setText(String.valueOf(NPUPlatform.clearanceOnWall));
        clearanceOnWall4.setText(String.valueOf(NPUPlatform.clearanceOnWall));
        clearanceOnWall5.setText(String.valueOf(NPUPlatform.clearanceOnWall));
        clearanceOnWall6.setText(String.valueOf(NPUPlatform.clearanceOnWall));
        clearanceOnWall7.setText(String.valueOf(NPUPlatform.clearanceOnWall));

        clearanceOnStepSupport.setText(String.valueOf(NPUPlatform.clearanceOnStepSupports));
        clearanceOnStepSupport1.setText(String.valueOf(NPUPlatform.clearanceOnStepSupports));
        clearanceOnStepSupport2.setText(String.valueOf(NPUPlatform.clearanceOnStepSupports));

        clearanceOnStepMax.setText(String.valueOf(model.clearanceMaxNPU));
        clearanceOnStepMaxNum.setText(String.valueOf(model.clearanceNumberMaxNPU));
        clearanceOnStepMin.setText(String.valueOf(model.clearanceOnStepMinNPU));
        clearanceOnStepMinNum.setText(String.valueOf(model.clearanceNumberMinNPU));

        widthStair.setText(String.valueOf(StartController.widthStairInt));
        widthStair1.setText(String.valueOf(StartController.widthStairInt));
        widthStair2.setText(String.valueOf(StartController.widthStairInt));
        widthStair3.setText(String.valueOf(StartController.widthStairInt));
        widthStair4.setText(String.valueOf(StartController.widthStairInt));
        widthStair5.setText(String.valueOf(StartController.widthStairInt));
        widthStair6.setText(String.valueOf(StartController.widthStairInt));

        foldedSupports.setText(String.valueOf(NPUPlatform.foldedSupports));
        if (NPUPlatform.foldedSupports + NPUPlatform.passage > StartController.widthStairInt)
            widthStair5.setFill(Color.BLUE);
        foldedWallLowerBox.setText(String.valueOf(NPUPlatform.foldedWallLowerBox));
        if (NPUPlatform.foldedWallLowerBox + NPUPlatform.passage > StartController.widthStairInt)
            widthStair6.setFill(Color.BLUE);
        foldedWallUpperBox.setText(String.valueOf(NPUPlatform.foldedWallUpperBox));
        if (NPUPlatform.foldedWallUpperBox + NPUPlatform.passage > StartController.widthStairInt)
            widthStair2.setFill(Color.BLUE);

        widthPassingOnSupportLower.setText(String.valueOf(NPUPlatform.widthOnSupportsPassingLower));
        if(NPUPlatform.widthOnSupportsPassingLower + NPUPlatform.clearanceOnWall > StartController.widthStairInt)
            widthPassingOnSupportLower.setFill(Color.RED);
        widthPassingOnSupportUpper.setText(String.valueOf(NPUPlatform.widthOnSupportsPassingUpper));
        if(NPUPlatform.widthOnSupportsPassingUpper + NPUPlatform.clearanceOnWall > StartController.widthStairInt)
            widthPassingOnSupportUpper.setFill(Color.RED);
        widthPassingOnWallLower.setText(String.valueOf(NPUPlatform.widthOnWallPassingLower));
        if(NPUPlatform.widthOnWallPassingLower + NPUPlatform.clearanceOnWall > StartController.widthStairInt)
            widthPassingOnWallLower.setFill(Color.RED);
        widthPassingOnWallUpper.setText(String.valueOf(NPUPlatform.widthOnWallPassingUpper));
        if(NPUPlatform.widthOnWallPassingUpper + NPUPlatform.clearanceOnWall > StartController.widthStairInt)
            widthPassingOnWallUpper.setFill(Color.RED);

        widthSideOnSupportLower.setText(String.valueOf(NPUPlatform.widthOnSupportsSideLower));
        if(NPUPlatform.widthOnSupportsSideLower + NPUPlatform.clearanceOnWall > StartController.widthStairInt)
            widthSideOnSupportLower.setFill(Color.RED);
        widthSideOnSupportUpper.setText(String.valueOf(NPUPlatform.widthOnSupportsSideUpper));
        if(NPUPlatform.widthOnSupportsSideUpper + NPUPlatform.clearanceOnWall > StartController.widthStairInt)
            widthSideOnSupportUpper.setFill(Color.RED);
        widthSideOnWallLower.setText(String.valueOf(NPUPlatform.widthOnWallSideLower));
        if(NPUPlatform.widthOnWallSideLower + NPUPlatform.clearanceOnWall > StartController.widthStairInt)
            widthSideOnWallLower.setFill(Color.RED);
        widthSideOnWallUpper.setText(String.valueOf(NPUPlatform.widthOnWallSideUpper));
        if(NPUPlatform.widthOnWallSideUpper + NPUPlatform.clearanceOnWall > StartController.widthStairInt)
            widthSideOnWallUpper.setFill(Color.RED);

        angleWay.setText(String.format("%.1f", model.angleNPU));
        angleWay1.setText(String.format("%.1f", model.angleNPU));
        if (model.angleNPU > NPUPlatform.angleMax) {
            angleWay.setFill(Color.RED);
            angleWay1.setFill(Color.RED);
        }

        heightStair.setText(String.valueOf(model.lengthStepsY.get(model.lengthStepsY.size())));
        heightStair1.setText(String.valueOf(model.lengthStepsY.get(model.lengthStepsY.size())));

        if (model.lengthStepsX.size() > 0) {
            lengthStair.setText(String.valueOf(model.lengthStepsX.get(model.lengthStepsX.size())));
            lengthStair1.setText(String.valueOf(model.lengthStepsX.get(model.lengthStepsX.size())));
        } else {
            lengthStair.setText("0");
            lengthStair1.setText("0");
        }

        lengthWayStair.setText(String.valueOf(model.lengthStair()));
        lengthWayStair1.setText(String.valueOf(model.lengthStair()));

        minUpperPlatformLower.setText(String.valueOf(NPUPlatform.minUpperPlatformLower));
        minUpperPlatformUpper.setText(String.valueOf(NPUPlatform.minUpperPlatformUpper));

        lengthUpperPlatform.setText(String.valueOf(StartController.lengthUpperPlatformInt));
        if (NPUPlatform.minUpperPlatformUpper > StartController.lengthUpperPlatformInt) {
            lengthUpperPlatform.setFill(Color.RED);
        }
        lengthUpperPlatform1.setText(String.valueOf(StartController.lengthUpperPlatformInt));
        if (NPUPlatform.minUpperPlatformLower > StartController.lengthUpperPlatformInt) {
            lengthUpperPlatform1.setFill(Color.RED);
        }

        lengthWay.setText(String.valueOf(model.lengthWayNPU));
        lengthWay1.setText(String.valueOf(model.lengthWayNPU));
        if (model.lengthWayNPU > NPUPlatform.lengthWayMax) {
            lengthWay.setFill(Color.RED);
            lengthWay1.setFill(Color.RED);
        }
        if (model.lengthWayNPU <= NPUPlatform.lengthWayMax && model.lengthWayNPU >= NPUPlatform.lengthWayMaxDU) {
            lengthWay.setFill(Color.BLUE);
            lengthWay1.setFill(Color.BLUE);
        }

        lengthLowerPlatform.setText(String.valueOf(StartController.lengthLowerPlatformInt));
        lengthLowerPlatform1.setText(String.valueOf(StartController.lengthLowerPlatformInt));

        lengthSide.setText(String.valueOf(model.lengthWayOnLowerPlatsNPU + NPUPlatform.lengthSide + NPUPlatform.clearanceOnWall));
        lengthSide1.setText(String.valueOf(model.lengthWayOnLowerPlatsNPU + NPUPlatform.lengthSide + NPUPlatform.clearanceOnWall));
        if (model.lengthWayOnLowerPlatsNPU + NPUPlatform.lengthSide + NPUPlatform.clearanceOnWall > StartController.lengthLowerPlatformInt) {
            lengthSide.setFill(Color.RED);
            lengthSide1.setFill(Color.RED);
        }

        lengthPassing.setText(String.valueOf(model.lengthWayOnLowerPlatsNPU + NPUPlatform.lengthPassing + NPUPlatform.clearanceOnWall));
        lengthPassing1.setText(String.valueOf(model.lengthWayOnLowerPlatsNPU + NPUPlatform.lengthPassing + NPUPlatform.clearanceOnWall));
        if (model.lengthWayOnLowerPlatsNPU + NPUPlatform.lengthPassing + NPUPlatform.clearanceOnWall > StartController.lengthLowerPlatformInt) {
            lengthPassing.setFill(Color.RED);
            lengthPassing1.setFill(Color.RED);
        }

        backButton.setOnAction(actionEvent -> {
            backButton.getScene().getWindow().hide();
            try {
                openNewScene();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    public void openNewScene() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(ResultController.class.getResource("result-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 1024, 868);
        Image icon = new Image("stair_l.png");
        Stage stage = new Stage();
        stage.getIcons().add(icon);
        stage.setTitle("РЕЗУЛЬТАТ");
        stage.setScene(scene);
        stage.show();
    }
}