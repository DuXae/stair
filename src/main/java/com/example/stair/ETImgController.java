package com.example.stair;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.example.stair.Platform.ETPlatform;
import com.example.stair.model.PlatformModel;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class ETImgController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Text angleET;

    @FXML
    private Text angleET1;

    @FXML
    private Text angleStair;

    @FXML
    private Text angleStair1;

    @FXML
    private Button backButton;

    @FXML
    private Text heightStair;

    @FXML
    private Text heightStair1;

    @FXML
    private Text lengthLowerPlatform;

    @FXML
    private Text lengthLowerPlatform1;

    @FXML
    private Text lengthStair;

    @FXML
    private Text lengthStair1;

    @FXML
    private Text lengthStairX;

    @FXML
    private Text lengthStairX1;

    @FXML
    private Text lengthUpperPlatform;

    @FXML
    private Text lengthUpperPlatform1;

    @FXML
    private Text lengthWay;

    @FXML
    private Text lengthWay1;

    @FXML
    private Text lowerPlatform;

    @FXML
    private Text lowerPlatform1;

    @FXML
    private Text upperPlatformMin;

    @FXML
    private Text upperPlatformMin1;

    @FXML
    private Text widthStair;

    @FXML
    private Text widthStair1;

    @FXML
    private Text widthStair2;

    @FXML
    private Text widthStair3;

    @FXML
    private Text widthStair4;

    @FXML
    private Text widthStair5;

    @FXML
    private Text catcher;

    @FXML
    private Text stepClearanceMax;

    @FXML
    private Text stepNumberMax;

    @FXML
    private Text lengthET;

    @FXML
    private Text foldedWall;

    @FXML
    private Text foldedSupports;

    @FXML
    private Text clearanceOnWall;

    @FXML
    private Text clearanceOnWall1;

    @FXML
    private Text clearanceOnWall2;

    @FXML
    private Text clearanceOnWall3;

    @FXML
    private Text clearanceOnStepSupports;

    @FXML
    private Text clearanceOnStepSupports1;

    @FXML
    private Text clearanceOnStepSupports2;

    @FXML
    private Text widthOnWallSide;

    @FXML
    private Text widthOnSupportsSide;

    @FXML
    private Text widthOnSupportsPassing;

    @FXML
    private Text widthOnWallPassing;

    @FXML
    private Text widthOnSupportsLowerPassing;

    @FXML
    private Text widthOnWallLowerSide;

    @FXML
    void initialize() {
        PlatformModel model = new PlatformModel();
        model.startModel();

        widthOnWallLowerSide.setText(String.valueOf(ETPlatform.widthOnWallLowerSide));
        widthOnSupportsLowerPassing.setText(String.valueOf(ETPlatform.widthOnSupportsLowerSide));
        widthOnWallPassing.setText(String.valueOf(ETPlatform.widthOnWallPassing));
        widthOnSupportsPassing.setText(String.valueOf(ETPlatform.widthOnSupportsPassing - ETPlatform.clearanceOnStepSupports));
        widthOnSupportsSide.setText(String.valueOf(ETPlatform.widthOnSupportsSide));
        widthOnWallSide.setText(String.valueOf(ETPlatform.widthOnWallSide));

        foldedWall.setText(String.valueOf(ETPlatform.foldedWall));
        foldedSupports.setText(String.valueOf(ETPlatform.foldedSupports));

        clearanceOnWall.setText(String.valueOf(ETPlatform.clearanceOnWall));
        clearanceOnWall1.setText(String.valueOf(ETPlatform.clearanceOnWall));
        clearanceOnWall2.setText(String.valueOf(ETPlatform.clearanceOnWall));
        clearanceOnWall3.setText(String.valueOf(ETPlatform.clearanceOnWall));

        clearanceOnStepSupports.setText(String.valueOf(ETPlatform.clearanceOnStepSupports));
        clearanceOnStepSupports1.setText(String.valueOf(ETPlatform.clearanceOnStepSupports));
        clearanceOnStepSupports2.setText(String.valueOf(ETPlatform.clearanceOnStepSupports));

        angleET.setText(String.valueOf(model.angleET));
        angleET1.setText(String.valueOf(model.angleET));
        if (model.angleET >= ETPlatform.catcher && model.angleET <= ETPlatform.angleMax) {
            angleET.setFill(Color.BLUE);
            angleET1.setFill(Color.BLUE);
            catcher.setText("ДА");
            catcher.setFill(Color.BLUE);
        } else if (model.angleET > ETPlatform.angleMax) {
            angleET.setFill(Color.RED);
            angleET1.setFill(Color.RED);
        }

        stepClearanceMax.setText(String.valueOf(model.clearanceMaxET));
        stepNumberMax.setText(String.valueOf(model.clearanceNumberMaxET));

        angleStair.setText(String.valueOf(model.angle).substring(0, 4));
        angleStair1.setText(String.valueOf(model.angle).substring(0, 4));

        widthStair.setText(String.valueOf(StartController.widthStairInt));
        if (StartController.widthStairInt < ETPlatform.passage + ETPlatform.foldedWall) {
            widthStair.setFill(Color.RED);
        }
        widthStair1.setText(String.valueOf(StartController.widthStairInt));
        if (StartController.widthStairInt < ETPlatform.widthOnSupportsSide + ETPlatform.clearanceOnWall) {
            widthStair1.setFill(Color.RED);
        }
        widthStair2.setText(String.valueOf(StartController.widthStairInt));
        if (StartController.widthStairInt < ETPlatform.widthOnWallSide + ETPlatform.clearanceOnWall) {
            widthStair2.setFill(Color.RED);
        }
        widthStair3.setText(String.valueOf(StartController.widthStairInt));
        if (StartController.widthStairInt < ETPlatform.passage + ETPlatform.foldedSupports) {
            widthStair3.setFill(Color.RED);
        }
        widthStair4.setText(String.valueOf(StartController.widthStairInt));
        if (StartController.widthStairInt < ETPlatform.widthOnSupportsPassing + ETPlatform.clearanceOnWall) {
            widthStair4.setFill(Color.RED);
        }
        widthStair5.setText(String.valueOf(StartController.widthStairInt));
        if (StartController.widthStairInt < ETPlatform.widthOnWallPassing + ETPlatform.clearanceOnWall) {
            widthStair5.setFill(Color.RED);
        }

        heightStair.setText(String.valueOf(model.lengthStepsY.get(model.lengthStepsY.size())));
        heightStair1.setText(String.valueOf(model.lengthStepsY.get(model.lengthStepsY.size())));

        lengthStair.setText(String.valueOf(model.lengthStair()));
        lengthStair1.setText(String.valueOf(model.lengthStair()));

        lengthWay.setText(String.valueOf(model.lengthWayET));
        lengthWay1.setText(String.valueOf(model.lengthWayET));
        if (model.lengthStepsX.size() > 0) {
            lengthStairX.setText(String.valueOf(model.lengthStepsX.get(model.lengthStepsX.size())));
            lengthStairX1.setText(String.valueOf(model.lengthStepsX.get(model.lengthStepsX.size())));
        } else {
            lengthStairX.setText("0");
            lengthStairX1.setText("0");
        }

        lengthET.setText(String.valueOf(model.lengthWayET + ETPlatform.lengthTrackAdd));
        if (model.lengthWayET > ETPlatform.lengthWayMax) {
            lengthET.setFill(Color.RED);
        }

        lowerPlatform.setText(String.valueOf(StartController.lengthLowerPlatformInt));
        lowerPlatform1.setText(String.valueOf(StartController.lengthLowerPlatformInt));

        lengthUpperPlatform.setText(String.valueOf(StartController.lengthUpperPlatformInt));
        lengthUpperPlatform1.setText(String.valueOf(StartController.lengthUpperPlatformInt));

        if (StartController.lengthUpperPlatformInt < ETPlatform.lengthRamp - model.lengthClearanceRampET) {
            lengthUpperPlatform.setFill(Color.RED);
            lengthUpperPlatform1.setFill(Color.RED);
        }

        upperPlatformMin.setText(String.valueOf(ETPlatform.lengthRamp - model.lengthClearanceRampET));
        upperPlatformMin1.setText(String.valueOf(ETPlatform.lengthRamp - model.lengthClearanceRampET));

        lengthLowerPlatform.setText(String.valueOf(model.lengthWayOnLowerPlatsET + ETPlatform.lengthSide + ETPlatform.clearanceOnWall));
        lengthLowerPlatform1.setText(String.valueOf(model.lengthWayOnLowerPlatsET + ETPlatform.lengthPassing + ETPlatform.clearanceOnWall));
        if (model.lengthWayOnLowerPlatsET + ETPlatform.lengthSide + ETPlatform.clearanceOnWall > StartController.lengthLowerPlatformInt) {
            lowerPlatform.setFill(Color.RED);
        }
        if (model.lengthWayOnLowerPlatsET + ETPlatform.lengthPassing + ETPlatform.clearanceOnWall > StartController.lengthLowerPlatformInt) {
            lowerPlatform1.setFill(Color.RED);
        }

        backButton.setOnAction(actionEvent -> {
            backButton.getScene().getWindow().hide();
            try {
                openNewScene();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    public void openNewScene() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(ResultController.class.getResource("result-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 1024, 868);
        Image icon = new Image("stair_l.png");
        Stage stage = new Stage();
        stage.getIcons().add(icon);
        stage.setTitle("РЕЗУЛЬТАТ");
        stage.setScene(scene);
        stage.show();
    }
}